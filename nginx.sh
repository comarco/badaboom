#!/bin/bash

########
####
# Configurer le virtual host Nginx
cat > /tmp/$nom_de_site <<NGINX_SCRIPT
map \$http_x_forwarded_proto \$https_flag {
  default off;
  https on;
}

server {
  server_name $nom_de_domaine www.$nom_de_domaine ;

  root $file_system_path;

	listen 443 ssl;

  ## Access and error logs.
  access_log /var/log/nginx/$nom_de_site-access-443.log ;
  error_log /var/log/nginx/$nom_de_site-error-443.log;

  index index.php;
 
  location = /favicon.ico {
    log_not_found off;
    access_log off;
  }

  location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
  }

  location / {
    try_files \$uri \$uri/ /index.php?\$args;
  }

  location ~ \.php$ {


## 2. Nginx FCGI specific directives.
#  fastcgi_buffers 256 4k;
#  fastcgi_intercept_errors on;

## Allow 4 hrs - pass timeout responsibility to upstream.
  fastcgi_index index.php;
  
  fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
}

  location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
    expires max;
    log_not_found off;
  }

#  ssl_certificate /etc/letsencrypt/live/$nom_de_domaine/fullchain.pem;
#  ssl_certificate_key /etc/letsencrypt/live/$nom_de_domaine/privkey.pem;
}

server { 
  listen 80;
  server_name $nom_de_domaine www.$nom_de_domaine;
  
  ## Access and error logs.
  access_log /var/log/nginx/$nom_de_site-access-80.log;
  error_log /var/log/nginx/$nom_de_site-error-80.log;

  return 301 https://$server_name$request_uri;
}
NGINX_SCRIPT

sudo mv /tmp/$nom_de_site /etc/nginx/sites-available/$nom_de_site
sudo chown root:root /etc/nginx/sites-available/$nom_de_site

echo "Nginx vhost ajouté"

# Activer le vhost
sudo ln -s /etc/nginx/sites-available/$nom_de_site /etc/nginx/sites-enabled/

echo "Vhost activé....."

# Redémarrer Nginx pour appliquer les modifications
nginx -t

echo "La configuration du vhost Nginx avec le certificat SSL a été terminée avec succès !"
