#!/bin/bash

########
####
### Variables
nom_de_domaine="$7"
nom_de_site="$4"
nom_de_base_de_donnee="$5"
nom_d_utilisateur_de_base_de_donnee="$6"
mot_de_passe_de_base_de_donnee="$8"
root_mysql_user="root"
root_mysql_password="$9"
file_system_path="/srv/vhosts/wp/$nom_de_site"
wp_config_file="$file_system_path/wp-config.php"

cd $file_system_path

wp db drop --yes

echo "La base de données MySQL a été détruite !"

cd ~/

sudo rm -rf $file_system_path

echo "Les fichiers ont été détruit !"

sudo rm /etc/nginx/sites-enabled/$nom_de_site
sudo rm /etc/nginx/sites-available/$nom_de_site

echo "Les vhost nginx ont té détruits1 !"