#!/bin/bash

########
####
# Configurer le virtual host Nginx
sudo rm /etc/nginx/sites-available/$nom_de_site 
sudo rm /etc/nginx/sites-enabled/$nom_de_site

cat > /tmp/$nom_de_site <<NGINX_SCRIPT
server {
    server_name $nom_de_domaine;

    root $file_system_path;

    listen 443 ssl;

    ## Access and error logs.
    access_log /var/log/nginx/$nom_de_site-access-443.log ;
    error_log /var/log/nginx/$nom_de_site-error-443.log;

    index index.php;

    #### Exclusions
    # Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
    # Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
    location ~* /\.(?!well-known\/) {
        deny all;
    }

    # Prevent access to certain file extensions
    location ~\.(ini|log|conf)$ {
        deny all;
    }

    # Deny access to any files with a .php extension in the uploads directory
    # Works in sub-directory installs and also in multisite network
    # Keep logging the requests to parse later (or to pass to firewall utilities such as fail2ban)
    location ~* /(?:uploads|files)/.*\.php$ {
        deny all;
    }

    #### Security
    # Generic security enhancements. Use https://securityheaders.io to test
    # and recommend further improvements.

    # Hide Nginx version in error messages and reponse headers.
    server_tokens off;

    # Don't allow pages to be rendered in an iframe on external domains.
    add_header X-Frame-Options "SAMEORIGIN" always;

    # MIME sniffing prevention
    add_header X-Content-Type-Options "nosniff" always;

    # Enable cross-site scripting filter in supported browsers.
    add_header X-Xss-Protection "1; mode=block" always;

    # Whitelist sources which are allowed to load assets (JS, CSS, etc). The following will block
    # only none HTTPS assets, but check out https://scotthelme.co.uk/content-security-policy-an-introduction/
    # for an in-depth guide on creating a more restrictive policy.
    # add_header Content-Security-Policy "default-src 'self' https: data: 'unsafe-inline' 'unsafe-eval';" always;

    #### Static files
    # Don't cache appcache, document html and data.
    location ~* \.(?:manifest|appcache|html?|xml|json)$ {
        expires 0;
    }

    # Cache RSS and Atom feeds.
    location ~* \.(?:rss|atom)$ {
        expires 1h;
    }

    # Caches images, icons, video, audio, HTC, etc.
    location ~* \.(?:jpg|jpeg|gif|png|ico|cur|gz|svg|mp4|ogg|ogv|webm|htc)$ {
        expires 1y;
        access_log off;
    }

    # Cache svgz files, but don't compress them.
    location ~* \.svgz$ {
        expires 1y;
        access_log off;
        gzip off;
    }

    # Cache CSS and JavaScript.
    location ~* \.(?:css|js)$ {
        expires 1y;
        access_log off;
    }

    # Cache WebFonts.
    location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
        expires 1y;
        access_log off;
        add_header Access-Control-Allow-Origin *;
    }

    # Don't record access/error logs for robots.txt.
    location = /robots.txt {
        try_files \$uri \$uri/ /index.php?\$args;
        access_log off;
        log_not_found off;
    }

    #### SSL
    # Generic SSL enhancements. Use https://www.ssllabs.com/ssltest/ to test
    # and recommend further improvements.

    # Don't use outdated SSLv3 protocol. Protects against BEAST and POODLE attacks.
    ssl_protocols TLSv1.2;

    # Use secure ciphers
    # ssl_ciphers EECDH+CHACHA20:EECDH+AES;
    ssl_ecdh_curve X25519:prime256v1:secp521r1:secp384r1;
    ssl_prefer_server_ciphers on;

    # Define the size of the SSL session cache in MBs.
    # ssl_session_cache shared:SSL:10m;

    # Define the time in minutes to cache SSL sessions.
    ssl_session_timeout 1h;

    # Use HTTPS exclusively for 1 year, uncomment one. Second line applies to subdomains.
    add_header Strict-Transport-Security "max-age=31536000;";
    # add_header Strict-Transport-Security "max-age=31536000; includeSubdomains;";

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_param  QUERY_STRING       \$query_string;
        fastcgi_param  REQUEST_METHOD     \$request_method;
        fastcgi_param  CONTENT_TYPE       \$content_type;
        fastcgi_param  CONTENT_LENGTH     \$content_length;

        fastcgi_param  SCRIPT_NAME        \$fastcgi_script_name;
        fastcgi_param  SCRIPT_FILENAME    \$document_root/\$fastcgi_script_name;
        fastcgi_param  REQUEST_URI        \$request_uri;
        fastcgi_param  DOCUMENT_URI       \$document_uri;
        fastcgi_param  DOCUMENT_ROOT      \$document_root;
        fastcgi_param  SERVER_PROTOCOL    \$server_protocol;
        fastcgi_param  REQUEST_SCHEME     \$scheme;
        fastcgi_param  HTTPS              \$https if_not_empty;

        fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
        fastcgi_param  SERVER_SOFTWARE    nginx/\$nginx_version;

        fastcgi_param  REMOTE_ADDR        \$remote_addr;
        fastcgi_param  REMOTE_PORT        \$remote_port;
        fastcgi_param  SERVER_ADDR        \$server_addr;
        fastcgi_param  SERVER_PORT        \$server_port;
        fastcgi_param  SERVER_NAME        \$server_name;

        # PHP only, required if PHP was built with --enable-force-cgi-redirect
        fastcgi_param  REDIRECT_STATUS    200;

        # Use the php pool defined in the upstream variable.
        # See global/php-pool.conf for definition.
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
    }
    
    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires max;
        log_not_found off;
    }
}

server { 
  listen 80;
  server_name $nom_de_domaine;
  
  ## Access and error logs.
  access_log /var/log/nginx/$nom_de_site-access-80.log;
  error_log /var/log/nginx/$nom_de_site-error-80.log;

  return 301 https://$server_name$request_uri;
}
NGINX_SCRIPT

sudo mv /tmp/$nom_de_site /etc/nginx/sites-available/$nom_de_site
sudo chown root:root /etc/nginx/sites-available/$nom_de_site

echo "Nginx vhost ajouté........"

# Activer le vhost
sudo ln -sf /etc/nginx/sites-available/$nom_de_site /etc/nginx/sites-enabled/$nom_de_site

echo "Vhost activé....."

# Redémarrer Nginx pour appliquer les modifications
sudo nginx -t
