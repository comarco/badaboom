#!/bin/bash

########
####
#### Creer le repertoire

sudo mkdir -p $file_system_path

# Télécharger la dernière version de Wordpress

cd /tmp
wget https://wordpress.org/latest.zip
sudo unzip -q latest.zip
sudo rsync -qazP wordpress/ $file_system_path/
sudo rm -rf wordpress
sudo rm -f /etc/nginx/sites-enabled/$nom_de_site

echo "Les fichiers de wordpress sont en place..."

# Configurer le fichier wp-config.php
sudo mv $file_system_path/wp-config-sample.php $wp_config_file
sudo sed -i "s/database_name_here/$nom_de_base_de_donnee/g" $wp_config_file
sudo sed -i "s/username_here/$nom_d_utilisateur_de_base_de_donnee/g" $wp_config_file
sudo sed -i "s/password_here/$mot_de_passe_de_base_de_donnee/g" $wp_config_file

echo "Configuré wp..."

# Permissions.
sudo chown -R marco:www-data $file_system_path

cd $file_system_path/

sudo find . -type d -exec sudo chmod 775 {} \;
sudo find . -type f -exec chmod 664 {} \;

sudo chmod 775 -R $file_system_path/wp-content

#### install wordpress

wp core install --url=$nom_de_domaine --title="$nom_de_domaine" --admin_user=admin --admin_password=$wp_admin_passwd --admin_email=info@$nom_de_domaine

#### Install some themes and plugins
wp plugin install duplicator --activate
wp plugin install nextgen-gallery --activate
wp theme install blockline --activate
wp plugin install wordpress-importer --activate

# Supprimer les fichiers inutiles
sudo rm /tmp/latest.zip

cd ~/badaboom

echo "Supprimé les fichiers inutiles... latest.zip supprimé"
