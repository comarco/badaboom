#!/bin/bash

########
####
#### Ce script est dangereux. 
#### NE PAS UTILISER SANS DEMANDER L'AVIS D'UN PRO.
#### Très dangereux...
####
#### 1. Il fut d'abord pointer un domaine vers le serveur où on roule le script
#### 2. Ensuite tu roules le script sur le bon serveur. Tout les serveurs ne peuvent pas fonctionner.
####    Il faut un nginx configuré comme le mien, sinon il faut adapter au votre.
#### 3. Une fois roulé on crée un certificat. `sudo certbot --nginx -d $nom_de_domaine`
#### 4. Acceder à votre WP. Installer via le UI. C'est prêt.
#### 
#### 
#### ChatGPT a participé.
####
#### Le script marche uniquement sur un certain serveur 
#### C'est un peu brutal quand "ça marche pas ;)"
####
#### TODO:
####   [ ] Nouvelles configs baseés sur spinupwp.com
####   [*] Séparer en multiples fichiers
####
########
##
# Ce script peut être exécuté en utilisant les commandes suivantes :
# 
# chmod +x script.sh
# ./script.sh  nom_du_site \
#              nom_de_base_de_donnee \
#              nom_d_utilisateur_de_base_de_donnee \
#              nom_de_domaine \
#              mot_de_passe_de_base_de_donnee \
#              root_mysql_password

function usage(){
	printf "Utilisation du script :\n"
	printf "\t--nginx | --no-nginx\n"
	printf "\t--mysql | --no-mysql\n"
	printf "\t--wp | --no-wp\n"
        printf "\t--delete\n"
}

if [ $# -eq 0 ]
then
	usage
fi

# Demande des informations
read -p "Entrez le nom de domaine: " nom_de_domaine
read -p "Entrez le nom de site: " nom_de_site
read -p "Entrez le nom de la base de données: " nom_de_base_de_donnee
read -p "Entrez le nom de l'utilisateur de base de données: " nom_d_utilisateur_de_base_de_donnee
read -p "Entrez le mot de passe de la base de données: " mot_de_passe_de_base_de_donnee
read -p "Entrez le mot de passe root de la base de données: " root_mysql_password
read -p "Entrez le mot de passe de l'utilisateur 'admin': " wp_admin_passwd

# Affiche les informations entrées
echo "Nom de domaine: $nom_de_domaine" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Nom de site: $nom_de_site" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Nom de la base de données: $nom_de_base_de_donnee" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Nom de l'utilisateur de base de données" : $nom_d_utilisateur_de_base_de_donnee >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Mot de passe de la base de données: $mot_de_passe_de_base_de_donnee" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Les fichiers sont ici: /srv/vhosts/wp/$nom_de_site" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log
echo "Login: https://$nom_de_domaine/wp-login.php USER=admin PASS=$wp_admin_passwd" >> ~/$nom_de_site-wp-install-$nom_de_domaine-$nom_d_utilisateur_de_base_de_donnee.log

#### Variables
root_mysql_user="root"
file_system_path="/srv/vhosts/wp/$nom_de_site"
wp_config_file="$file_system_path/wp-config.php"

my_dir="$(dirname "$0")"

# Analyse les options
while [ "$1" != "" ]; do
    case $1 in
        --nginx)
            install_nginx=true
            ;;
        --no-nginx)
            install_nginx=false
            ;;
        --mysql)
            install_mysql=true
            ;;
        --no-mysql)
            install_mysql=false
            ;;
        --wp)
            install_wp=true
            ;;
        --no-wp)
            install_wp=false
            ;;
        --delete)
            delete=true
            ;;
        --ssl)
            ssl=true
            ;;
        *)
            echo "Option inconnue: $1"
            exit 1
            ;;
    esac
    shift
done

source $my_dir/logs.sh "$nom_de_domaine $nom_de_site $my_dir $nom_de_base_de_donnee $nom_d_utilisateur_de_base_de_donnee $file_system_path $root_mysql_user $root_mysql_password"

if [ "$install_mysql" = true ]; then
source $my_dir/mysql.sh "$nom_de_domaine $nom_de_site $my_dir $nom_de_base_de_donnee $nom_d_utilisateur_de_base_de_donnee $file_system_path $root_mysql_user $root_mysql_password" 
else
echo "Pas de DB...."
fi

if [ "$install_wp" = true ]; then
source $my_dir/wp.sh "$wp_admin_passwd $nom_de_domaine $nom_de_site $my_dir $nom_de_base_de_donnee $nom_d_utilisateur_de_base_de_donnee $file_system_path $root_mysql_user $root_mysql_password"
else
echo "Pas de reinstallation de wp...."
fi

if [ "$install_nginx" = true ]; then
source "$my_dir/nginx-new.sh" "$nom_de_domaine $nom_de_site $my_dir $nom_de_base_de_donnee $nom_d_utilisateur_de_base_de_donnee $file_system_path $wp_config_file $root_mysql_user $root_mysql_password"

sudo service nginx stop ; sudo service nginx start

echo "La configuration du vhost Nginx avec le certificat SSL a été terminée avec succès !"
else
echo "Pas de reinstallation de wp...."
fi

#### Delete
if [ "$delete" = true ]; then
  source $my_dir/delete-wp.sh 
fi

if [ "$ssl" = true ]; then
  sudo certbot --nginx -d $nom_de_domaine
fi

# Configurer le certificat SSL avec certbot
echo "------------"
echo "Rouler:"
echo "sudo certbot --nginx -d $nom_de_domaine"
echo "Visitez https://$nom_de_domaine"


