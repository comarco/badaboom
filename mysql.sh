#!/bin/bash

########
####

# Créer la base de données MySQL
mysql -u root -p$root_mysql_password <<MYSQL_SCRIPT
CREATE DATABASE $nom_de_base_de_donnee;
CREATE USER '$nom_d_utilisateur_de_base_de_donnee'@'localhost' IDENTIFIED BY '$mot_de_passe_de_base_de_donnee';
GRANT ALL PRIVILEGES ON $nom_de_base_de_donnee.* TO '$nom_d_utilisateur_de_base_de_donnee'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

echo "La base de données MySQL a été créée avec succès !"