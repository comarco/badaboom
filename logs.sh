#!/bin/bash

########
####

sudo cat > ~/$nom_de_site-wp-install-date-`date +%Y-%m-%d-%H-%M`.log <<LOGS
# Informations de connexion à la base de données
#
# Connexion à la base de données
# Nom de la base de données :  $nom_de_base_de_donnee
# Nom d'utilisateur :          $nom_d_utilisateur_de_base_de_donnee
# Mot de passe :               $mot_de_passe_de_base_de_donnee
# Hôte :                       localhost

# Informations d'authentification pour se 
# connecter en tant que root à la base de données
#

# Authentification root MySQL
# Nom d'utilisateur root :      XYZ #\$root_mysql_user
# Mot de passe root :           XYZ #\$root_mysql_password
#
LOGS

echo "Trouve le fichier de log dans ton \$home"